package com.malban.capp.test;

import com.malban.capp.config.SpringRootConfig;
import com.malban.capp.dao.UserDAO;
import com.malban.capp.domain.User;
import com.malban.capp.service.UserService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author HHA
 */
public class TestUserDAOSave {
	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringRootConfig.class);
		UserDAO userDao = ctx.getBean(UserDAO.class);

		// TODO: the user details will be taken from User-Reg-Form
		User u = new User();
		u.setName("May Thu");
		u.setPhone("098765432");
		u.setEmail("maythu@gmail.com");
		u.setAddress("Mandalay");
		u.setLoginName("mm");
		u.setPassword("mm123");
		u.setRole(UserService.ROLE_USER);// Admin Role
		u.setLoginStatus(UserService.LOGIN_STATUS_ACTIVE);// active
		userDao.save(u);
		System.out.println("---------------Data Saved ! ----------------");

	}  
}
