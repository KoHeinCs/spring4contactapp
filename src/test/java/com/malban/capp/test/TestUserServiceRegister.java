package com.malban.capp.test;

import com.malban.capp.config.SpringRootConfig;
import com.malban.capp.dao.UserDAO;
import com.malban.capp.domain.User;
import com.malban.capp.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author HHA
 */
public class TestUserServiceRegister {

	public static void main(String[] args) {
		
		ApplicationContext ctx=new AnnotationConfigApplicationContext(SpringRootConfig.class);
		
		UserService userService=ctx.getBean(UserService.class);
		
		// TODO: the user details will be taken from User-Reg-Form
		User u=new User();
			u.setName("Nay Htet");
			u.setPhone("098765432");
			u.setEmail("nayhtet@gmail.com");
			u.setAddress("Mandalay");
			u.setLoginName("nayhtet");
			u.setPassword("nayhtet123");
			u.setRole(UserService.ROLE_USER);// Role
			u.setLoginStatus(UserService.LOGIN_STATUS_ACTIVE);//active
			userService.register(u);
			System.out.println("---------------Register Success ! ----------------");
			
			
	}

}
