package com.malban.capp.test;

import com.malban.capp.config.SpringRootConfig;
import javax.sql.DataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author HHA
 */
public class TestDataSource {  
	public static void main(String agr[]) {
		
		ApplicationContext ctx=new AnnotationConfigApplicationContext(SpringRootConfig.class);
		DataSource ds=ctx.getBean(DataSource.class);
		JdbcTemplate jt=new JdbcTemplate(ds);
		String sql= "insert into user(`name`, `phone`, `email`, `address`, `loginName`, `password`) values(?,?,?,?,?,?)";
		Object[] param=new Object[] {"Vikram","09876654","vikram@gmail.com","Bhopal","v","v123"};
		jt.update(sql, param);
		System.out.println("--------------------SQL executed successfully ----------------------");
	} 
}
