package com.malban.capp.test;

import com.malban.capp.config.SpringRootConfig;
import com.malban.capp.dao.UserDAO;
import com.malban.capp.domain.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author HHA
 */
public class TestUserDAOUpdate {
 
	public static void main(String[] args) {
		
		ApplicationContext ctx=new AnnotationConfigApplicationContext(SpringRootConfig.class);
		UserDAO userDao=ctx.getBean(UserDAO.class);
		
		// TODO: the user details will be taken from User-Reg-Form
		User u=new User();
			u.setName("Hein Htet Aung");
			u.setPhone("098765432");
			u.setEmail("heinhtetaungcu@gmail.com");
			u.setAddress("Yangon");
			u.setRole(1);//Admin Role
			u.setLoginStatus(1);//active
			u.setUserId(3);
			userDao.update(u);
			System.out.println("---------------Data Updated ! ----------------");
			
		
}
	
}
