package com.malban.capp.test;

import com.malban.capp.config.SpringRootConfig;
import com.malban.capp.dao.UserDAO;
import com.malban.capp.domain.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author HHA
 */
public class TestUserDAOFindSingleRecord {

	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringRootConfig.class);
		UserDAO userDao = ctx.getBean(UserDAO.class);
		User u=userDao.findById(2);
		System.out.println("--------------- User Details ----------------");
		System.out.println(u.toString());
	}
}
