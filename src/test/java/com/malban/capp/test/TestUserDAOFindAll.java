package com.malban.capp.test;

import com.malban.capp.config.SpringRootConfig;
import com.malban.capp.dao.UserDAO;
import com.malban.capp.domain.User;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author HHA
 */
public class TestUserDAOFindAll {
	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringRootConfig.class);
		UserDAO userDao = ctx.getBean(UserDAO.class);
		List<User> users=userDao.findAll();
		System.out.println("--------------- User Details ----------------");
		for(User u:users) {
			System.out.println(u.toString());
		}
	}  
}
