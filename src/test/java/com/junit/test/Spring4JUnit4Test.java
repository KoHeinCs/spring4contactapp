package com.junit.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.malban.capp.config.SpringRootConfig;
import com.malban.capp.dao.UserDAO;

@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(classes = SpringRootConfig.class)// to know service and dao beans
public class Spring4JUnit4Test {
	
	@Autowired
	private UserDAO userDao;
	
	@Test
	public void getUser() {
		System.out.println(userDao.findById(1));
		//assertEquals("user", userDao.findById(1));
	}

}
