<%-- 
    Document   : index
    Created on : May 1, 2018, 3:59:52 PM
    Author     : MAMI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Contact List- Contact Application</title>
<s:url var="url_css" value="/static/css/style.css" />
<link href="${url_css}" rel="stylesheet" type="text/css" />
</head>
<s:url var="url_bg" value="/static/images/bg.jpg" />
<body background="${url_bg}">
	<table border="1" width="80%" align="center">
		<tr>
			<td height="80px">
				<%-- Header --%> <jsp:include page="include/header.jsp" />
			</td>
		</tr>
		<tr>
			<td height="25px">
				<%-- Menu --%> <jsp:include page="include/menu.jsp" />
			</td>
		</tr>
		<tr>
			<td height="350px" valign="top">
				<%-- Page Content Area--%>
				<h3>Contact List</h3> <c:if test="${err !=  null}">
					<p class="error">${err}</p>
				</c:if> <c:if test="${param.act eq 'sv'}">
					<p class="success">Contact Added Successfully ! Thanks for
						using contact application</p>
				</c:if> <c:if test="${param.act eq 'del'}">
					<p class="success">Contact Deleted Successfully ! Thanks for
						using contact application</p>
				</c:if> <c:if test="${param.act eq 'ed'}">
					<p class="success">Contact Edited Successfully ! Thanks for
						using contact application</p>
				</c:if>

				<form action="<s:url value="/user/contact_search" />">
					<input type="text" name="freeText" value="${param.freeText }"
						placeholder="Enter Text To Search ">
					<button>Find</button>
				</form><br/>

				<form action="<s:url value="/user/bulk_cdelete" />">
					<button>Delete Selected Records</button><br/><br>
					<table border="1">
						<thead>
							<tr>
								<th>Select</th>
								<th>Id</th>
								<th>Name</th>
								<th>Phone No</th>
								<th>Email</th>
								<th>Address</th>
								<th>Remark</th>
								<th colspan="2">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${empty contactList }">
								<tr>
									<td colspan="8" class="error" align="center">No Records
										Present</td>
								</tr>
							</c:if>
							<c:forEach items="${contactList}" var="contacts">

								<tr>
									<td align="center"><input type="checkbox" name="cid" value="${contacts.contactId}" /></td>
									<td><c:out value="${contacts.contactId}" /></td>
									<td><c:out value="${contacts.name}" /></td>
									<td><c:out value="${contacts.phone}" /></td>
									<td><c:out value="${contacts.email}" /></td>
									<td><c:out value="${contacts.adress}" /></td>
									<td><c:out value="${contacts.remark}" /></td>
									<s:url var="url_del" value="/user/del_contact">
										<s:param name="cid" value="${contacts.contactId}"></s:param>
									</s:url>
									<s:url var="url_edit" value="/user/edit_contact">
										<s:param name="cid" value="${contacts.contactId}"></s:param>
									</s:url>
									<td><a href="${url_edit}">Edit</a> | <a href="${url_del}">Delete</a></td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</form>




			</td>
		</tr>
		<tr>
			<td height="25px">
				<%-- Footer --%> <jsp:include page="include/footer.jsp" />
			</td>
		</tr>
	</table>
</body>
</html>
