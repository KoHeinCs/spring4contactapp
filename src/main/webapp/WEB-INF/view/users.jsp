<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User  List - Contact Application </title>
         <s:url var="url_css" value="/static/css/style.css"/>
         <s:url var="url_jqlib" value="/static/js/jquery-3.4.1.min.js"/>
         <script src="${url_jqlib}"></script>
         <script type="text/javascript">
         	function changeStatus(uid,lstatus) {
				//alert(userId+"="+loginStatus)
				$.ajax({
					url:'change_status',
					data:{userId:uid,loginStatus:lstatus},
					success:function(data){
						alert(data)
					}
				});
			}
         </script>
        <link href="${url_css}" rel="stylesheet" type="text/css"/>
    </head>
    <s:url var="url_bg" value="/static/images/bg.jpg"/>
    <body background="${url_bg}">
        <table border="1" width="80%" align="center">
            <tr>
                <td height="80px">
                    <%-- Header --%>
                    <jsp:include page="include/header.jsp"/>
                </td>
            </tr>
            <tr>
                <td height="25px">
                     <%-- Menu --%>
                     <jsp:include page="include/menu.jsp"/>
                </td>
            </tr>
            <tr>
                <td height="350px" valign="top">
                     <%-- Page Content Area--%>
                     <h3>User List</h3>
                  
                    <table border="1">
						<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Phone No</th>
								<th>Email</th>
								<th>Address</th>
								<th>User Name</th>
								<th>Status</th>
								
							</tr>
						</thead>
						<tbody>
							<c:if test="${empty userList }">
								<tr>
									<td colspan="8" class="error" align="center">No Records
										Present</td>
								</tr>
							</c:if>
							<c:forEach items="${userList}" var="u">

								<tr>
									
									<td><c:out value="${u.userId}" /></td>
									<td><c:out value="${u.name}" /></td>
									<td><c:out value="${u.phone}" /></td>
									<td><c:out value="${u.email}" /></td>
									<td><c:out value="${u.address}" /></td>
									<td><c:out value="${u.loginName}" /></td>
									<td>
										<select id="id_${u.userId}" onchange="changeStatus(${u.userId},$(this).val())">
											<option value="1">Active</option>
											<option value="2">Block</option>
										</select>
										<script >
											$('#id_${u.userId}').val(${u.loginStatus})
										</script>
									</td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
                </td>
            </tr>
             <tr>
                <td height="25px">
                     <%-- Footer --%>
                     <jsp:include page="include/footer.jsp"/>
                </td>
            </tr>
        </table>
    </body>
</html>
