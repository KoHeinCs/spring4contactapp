
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>


 <s:url var="url_logout" value="/logout"/>
  <s:url var="url_reg" value="/reg_form"/>
   <s:url var="url_index" value="/index"/>
   <s:url var="url_chome" value="/user/dashboard"/>
    <s:url var="url_cform" value="/user/contact_form"/>
     <s:url var="url_clist" value="/user/clist"/>
     <s:url var="url_ulist" value="/admin/users"/>
     <s:url var="url_ahome" value="/admin/dashboard"/>
    
 

<c:if test="${sessionScope.userId==null}">
<%--User is not yet logged in  : Guest Menu --%>
<a href="#"><font color="FF00CC">Home</font></a> | <a href="${url_index}"><font color="FF00CC">Login</font></a> | <a href="${url_reg}"><font color="FF00CC">Register</font></a> | <a href="#"><font color="FF00CC">About</font></a> 
</c:if>

<c:if test="${sessionScope.userId!=null && sessionScope.role==1}">
<%--Admin is logged in  : Admin Menu --%>
<a href="${url_ahome}"><font color="FF00CC">Home</font></a> | <a href="${url_ulist}"><font color="FF00CC">User List</font></a> | <a href="${url_logout}"><font color="FF00CC">Logout</font></a> 
</c:if>

<c:if test="${sessionScope.userId!=null && sessionScope.role==2}">
<%--User is logged in  : User Menu --%>
<a href="${url_chome}"><font color="FF00CC">Home</font></a> | <a href="${url_cform}"><font color="FF00CC">Add Contact</font></a> | <a href="${url_clist}"><font color="FF00CC">Contact List</font></a> | <a href="${url_logout}"><font color="FF00CC">Logout</font></a> 
</c:if>

