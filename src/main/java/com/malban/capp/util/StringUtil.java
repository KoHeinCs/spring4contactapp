package com.malban.capp.util;

/**
 * This class contains utility methods related to String Operations.
 * @author HHA
 */
public class StringUtil {

	public static String toCommaSeperatedString(Object[] items) {
		
		StringBuilder sb=new StringBuilder();
		for(Object item:items) {
			sb.append(item).append(",");
		}
		if(sb.length()>0) {
			sb.deleteCharAt(sb.length()-1);
		}
		return sb.toString();
	}
}
