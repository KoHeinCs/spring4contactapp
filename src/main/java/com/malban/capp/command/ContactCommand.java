package com.malban.capp.command;

import com.malban.capp.domain.Contact;

public class ContactCommand {
Contact contact;

public Contact getContact() {
	return contact;
}

public void setContact(Contact contact) {
	this.contact = contact;
}

}
