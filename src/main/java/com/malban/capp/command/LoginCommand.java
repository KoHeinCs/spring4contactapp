package com.malban.capp.command;

/**
 *
 * @author HHA
 */
public class LoginCommand {
    private String loginName;
    private String password;    

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	@Override
	public String toString() {
		return "LoginCommand [loginName=" + loginName + ", password=" + password + "]";
	}
    
    
    
}
