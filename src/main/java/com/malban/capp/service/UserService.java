package com.malban.capp.service;

import com.malban.capp.domain.User;
import com.malban.capp.exception.UserBlockedException;
import java.util.List;

/**
 *
 * @author HHA
 */
public interface UserService {
    
	public static final Integer LOGIN_STATUS_ACTIVE=1;
	public static final Integer LOGIN_STATUS_BLOCKED=2;
	
	public static final Integer ROLE_ADMIN=1;
	public static final Integer ROLE_USER=2;	
	
	
	/*
	 * This method handle the user registration task 
	 */
	public void register(User user);

	/*
	 * this method handles login operation(authentication)  , 
	 * return user  object when success  and null when failed.
	 * When user account is blocked an exception will be thrown by this method
	 * @param loginName
	 * @param password
	 * @throws in.ezeon.capp.exception.UserBlockedException
	 * 
	 */
	public User login(String loginName, String password) throws UserBlockedException;
	
	
	/*
	 * this method to get list of registered user 
	 * @return
	 */
	
	public List<User> getUserList();
	
	/*
	 * This method change the user login status detail passed as arguments . 
	 * @param userId
	 * @param loginStatus
	 */
	public void changeLoginStatus(Integer userId,Integer loginStatus);
   
}
