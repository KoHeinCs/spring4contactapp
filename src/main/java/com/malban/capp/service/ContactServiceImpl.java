package com.malban.capp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.malban.capp.dao.BaseDAO;
import com.malban.capp.dao.ContactDAO;
import com.malban.capp.domain.Contact;
import com.malban.capp.rm.ContactRowMapper;
import com.malban.capp.util.StringUtil;

/**
 *
 * @author HHA
 */
@Service
public class ContactServiceImpl extends BaseDAO implements ContactService{
	
	@Autowired
	public ContactDAO contactDao;

	public void save(Contact c) {
		contactDao.save(c);
	}

	public void update(Contact c) {
		contactDao.update(c);

	}

	public void delete(Integer contactId) {
		contactDao.delete(contactId);
	}

	public void delete(Integer[] contactIds) {
		String ids=StringUtil.toCommaSeperatedString(contactIds);
		String sql="delete from contact where contactId in("+ids+")";
		getJdbcTemplate().update(sql);

	}

	public List<Contact> findUserContact(Integer userId) {
		
		return contactDao.findByProperty("userId", userId);
	}

	public List<Contact> findUserContact(Integer userId, String txt) {
		String sql="select contactId,userId,name,phone,email,adress,remark from contact where userId=? and (name like '%"+txt+"%' or adress like '%"+txt+"%' or phone like '%"+txt+"%'  or email like '%"+txt+"%' or remark like '%"+txt+"%')";
		return getJdbcTemplate().query(sql, new ContactRowMapper(),userId);
	}

	@Override
	public Contact findById(Integer contactId) {
		
		return contactDao.findById(contactId);
	}
    
  
}
