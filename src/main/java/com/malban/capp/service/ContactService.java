package com.malban.capp.service;

import com.malban.capp.domain.Contact;
import java.util.List;

/**
 * The interface specifies all business operation for Contact Entity.
 *
 * @author HHA
 */
public interface ContactService {
	public void save(Contact c);
	public void update(Contact c);
	public void delete(Integer contactId);
	public void delete(Integer[] contactIds);
	public Contact findById(Integer contactId);
	
	/*
	 * This method return all user contact (user who logged in )
	 * @param userId
	 * @return
	 */
	public List<Contact> findUserContact(Integer userId);
	
	/*
	 * This method search for user based on given text 
	 */
	public List<Contact> findUserContact(Integer userId,String txt);
}
