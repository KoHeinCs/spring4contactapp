package com.malban.capp.service;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.malban.capp.dao.BaseDAO;
import com.malban.capp.dao.UserDAO;
import com.malban.capp.domain.User;
import com.malban.capp.exception.UserBlockedException;
import com.malban.capp.rm.UserRowMapper;

/**
 *
 * @author HHA
 */
@Service
public class UserServiceImpl extends BaseDAO implements UserService {

	@Autowired
	private UserDAO userDao;


	public void register(User user) {
		userDao.save(user);
	}

	public User login(String loginName, String password) throws UserBlockedException {
		String sql="select userId,name,phone,email,address,role,loginStatus,loginName "
				+" from user where loginName=:ln and password=:pw";
		Map m=new HashMap();
		m.put("ln", loginName);
		m.put("pw", password);
		
		try {
			User u = getNamedParameterJdbcTemplate().queryForObject(sql,m,new UserRowMapper());
			if(u.getLoginStatus().equals(UserService.LOGIN_STATUS_BLOCKED)) {
				throw new UserBlockedException("Your account has been blocked . Contact to admin ");
			}else {
				return u;
			}
		
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	
	}

	public List<User> getUserList() {
		return userDao.findByProperty("role", UserService.ROLE_USER);
	}

	public void changeLoginStatus(Integer userId, Integer loginStatus) {
		String sql="update user set loginStatus=:lst where userId=:uid";
		Map m=new HashMap();
		m.put("lst", loginStatus);
		m.put("uid", userId);
		getNamedParameterJdbcTemplate().update(sql,m);
	}
}
