package com.malban.capp.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.malban.capp.command.ContactCommand;
import com.malban.capp.dao.ContactDAO;
import com.malban.capp.domain.Contact;
import com.malban.capp.service.ContactService;

@Controller
public class ContactController {

	@Autowired
	private ContactService contactService;
	
	@Value("${hha.val}")
	private String hello;

	@RequestMapping(value = "/user/contact_form", method = RequestMethod.GET)
	public String contactForm(Model m) {
		m.addAttribute("command", new Contact());
		return "contact_form";
	}

	@RequestMapping(value = "/user/save_contact")
	public String saveOrUpdateContact(@ModelAttribute("command") Contact c, HttpSession session, Model m) {
		Integer contactId = (Integer) session.getAttribute("aContactId");// get from contact edit function's session
		System.out.println("contact id >> "+contactId);
		if (contactId == null) {
			
			// save tasks
			try {
				Integer userId = (Integer) session.getAttribute("userId");// get from login user's session
				c.setUserId(userId); // need to save
				contactService.save(c);
				return "redirect:clist?act=sv";// redirect to contact list

			} catch (Exception e) {
				e.printStackTrace();
				m.addAttribute("err", "Failed to save contact ! ");
				return "redirect:contact_form";
			}

		}else {
			try {
				c.setContactId(contactId); // 
				contactService.update(c);
				return "redirect:clist?act=ed";// redirect to contact list

			} catch (Exception e) {
				e.printStackTrace();
				m.addAttribute("err", "Failed to edit contact ! ");
				return "redirect:contact_form";
			}
		}
		
	}

	@RequestMapping(value = "/user/clist", method = RequestMethod.GET)
	public String contactList(Model m, HttpSession session) {
		Integer userId = (Integer) session.getAttribute("userId");
		m.addAttribute("contactList", contactService.findUserContact(userId));
		return "clist";
	}
	
	@RequestMapping(value = "/user/contact_search", method = RequestMethod.GET)
	public String contactSearchList(Model m, HttpSession session,@RequestParam("freeText") String freeText) {
		Integer userId = (Integer) session.getAttribute("userId");
		m.addAttribute("contactList", contactService.findUserContact(userId, freeText));
		return "clist";
	}

	@RequestMapping(value = "/user/del_contact", method = RequestMethod.GET)
	public String deleteContact(@RequestParam("cid") Integer contactId) {
		contactService.delete(contactId);
		return "redirect:clist?act=del";
	}
	
	@RequestMapping(value = "/user/bulk_cdelete", method = RequestMethod.GET)
	public String deleteBulkContact(@RequestParam("cid") Integer[] contactIds) {
		contactService.delete(contactIds);
		return "redirect:clist?act=del";
	}

	@RequestMapping(value = "/user/edit_contact")
	public String editContact(@RequestParam("cid") Integer contactId, Model m, HttpSession session) {
		session.setAttribute("aContactId", contactId);
		Contact c = contactService.findById(contactId);
		m.addAttribute("command", c);
		return "contact_form";
	}

}
