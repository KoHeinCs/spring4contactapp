package com.malban.capp.controller;

import javax.servlet.http.HttpSession;

//import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.malban.capp.command.LoginCommand;
import com.malban.capp.command.UserCommand;
import com.malban.capp.domain.User;
import com.malban.capp.exception.UserBlockedException;
import com.malban.capp.service.UserService;

/**
 *
 * @author HHA
 */
@Controller
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model m) {
		m.addAttribute("command", new LoginCommand());
		return "index";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String handleLogin(@ModelAttribute("command") LoginCommand cmd, Model m, HttpSession session) {

		try {

			User loggedInUser = userService.login(cmd.getLoginName(), cmd.getPassword());
			if (loggedInUser == null) {
				// FAILED
				// add error message and go back to login-form
				m.addAttribute("err", "Login Failed! Enter valid credentials.");
				return "index";// JSP - Login FORM
			} else // SUCCESS
			// check the role and redirect to a appropriate dashboard
			{
				if (loggedInUser.getRole().equals(UserService.ROLE_ADMIN)) {
					// add user detail in session (assign session to logged in user)
					addUserInSession(loggedInUser, session);
					return "redirect:admin/dashboard";
				} else if (loggedInUser.getRole().equals(UserService.ROLE_USER)) {
					// add user detail in session (assign session to logged in user)
					addUserInSession(loggedInUser, session);
					return "redirect:user/dashboard";
				} else {
					// add error message and go back to login-form
					m.addAttribute("err", "Invalid User ROLE");
					return "index";// JSP - Login FORM
				}
			}
		} catch (UserBlockedException ex) {
			// add error message and go back to login-form
			m.addAttribute("err", ex.getMessage());
			return "index";// JSP - Login FORM
		}
	}

	@RequestMapping(value = "/user/dashboard", method = RequestMethod.GET)
	public String userDashboard() {
		return "dashboard_user";
	}

	@RequestMapping(value = "/admin/dashboard", method = RequestMethod.GET)
	public String adminDashboard() {
		return "dashboard_admin";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String loggout(HttpSession session) {
		session.invalidate();
		return "redirect:index?act=lo";
		// return "index";

	}
	
	@RequestMapping(value = "/reg_form",method = RequestMethod.GET)
	public String registrationForm(Model m) {
		m.addAttribute("command", new UserCommand());
		return "reg_form";
	}
	
	@RequestMapping(value = "/register")
	public String registerUser(@ModelAttribute("command") UserCommand cmd,Model m) {
		try {
			User user = cmd.getUser();
			user.setRole(userService.ROLE_USER);
			user.setLoginStatus(userService.LOGIN_STATUS_ACTIVE);
			userService.register(user);
			return "redirect:index?act=reg";
		} catch (DuplicateKeyException e) {
			e.printStackTrace();
			m.addAttribute("err", "User is already registered . Please select another username");
			return "reg_form";
		}
	}

	@RequestMapping(value = "/admin/users", method = RequestMethod.GET)
	public String userList(Model m) {
		m.addAttribute("userList", userService.getUserList());
		return "users";
	}
	
	@RequestMapping(value = "/admin/change_status", method = RequestMethod.GET)
	@ResponseBody
	public String changeUserStatus(@RequestParam Integer userId,@RequestParam Integer loginStatus) {
		System.out.println("userId : "+userId+" , loginStatus : "+loginStatus);
		try {
			userService.changeLoginStatus(userId, loginStatus);
			return "SUCCESS : Status Changed ";
		} catch (Exception e) {
			return "ERROR : Unable to change ";
		}
	}
	
	
	
	
	private void addUserInSession(User u, HttpSession session) {
		session.setAttribute("user", u);
		session.setAttribute("userId", u.getUserId());
		session.setAttribute("role", u.getRole());
		session.setMaxInactiveInterval(60*5);// loggout after 1 seconds
	}

}
