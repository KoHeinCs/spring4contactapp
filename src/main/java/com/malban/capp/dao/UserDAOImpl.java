package com.malban.capp.dao;

import com.malban.capp.domain.User;
import com.malban.capp.rm.UserRowMapper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author HHA
 */
@Repository
public class UserDAOImpl extends BaseDAO implements UserDAO {
	
	public void save(User user) {
		String sql= "insert into user(name,phone,email,address,loginName,password,role,loginStatus) "
				+ "values(:name,:phone,:email,:address,:loginName,:password,:role,:loginStatus)";
		Map m=new HashMap();
		m.put("name", user.getName());
		m.put("phone", user.getPhone());
		m.put("email", user.getEmail());
		m.put("address", user.getAddress());
		m.put("loginName", user.getLoginName());
		m.put("password", user.getPassword());
		m.put("role", user.getRole());
		m.put("loginStatus", user.getLoginStatus());
		KeyHolder kh=new GeneratedKeyHolder();
		SqlParameterSource ps=new MapSqlParameterSource(m);
		super.getNamedParameterJdbcTemplate().update(sql, ps, kh);
		Integer userId=kh.getKey().intValue();
		user.setUserId(userId);
		
	}

	public void update(User user) {
		
		String sql=" update user "+
					" set name=:name, "+
					" phone=:phone, "+
					" email=:email, "+
					" address=:address, "+
					" role=:role, "+
					" loginStatus=:loginStatus "+
					" where userId=:userId";
		Map m=new HashMap();
		m.put("name", user.getName());
		m.put("phone", user.getPhone());
		m.put("email", user.getEmail());
		m.put("address", user.getAddress());
		m.put("role", user.getRole());
		m.put("loginStatus", user.getLoginStatus());
		m.put("userId", user.getUserId());
		getNamedParameterJdbcTemplate().update(sql,m);
		
		
					
		
		
	}
	
	public void delete(User user) {
		this.delete(user.getUserId());
		
	}


	public void delete(Integer userId) {
		String sql="delete from user where userId=?";
		getJdbcTemplate().update(sql,userId);
		
	}

	public User findById(Integer userId) {
		String sql="select userId,name,phone,email,address,loginName,role,loginStatus from user where userId=?";
		User u=getJdbcTemplate().queryForObject(sql,new UserRowMapper(), userId);
		return u;
	}

	public List<User> findAll() {
		String sql="select userId,name,phone,email,address,loginName,role,loginStatus from user";
		List<User> users= getJdbcTemplate().query(sql, new UserRowMapper());
		return users;
	}

	public List<User> findByProperty(String propName, Object propValue) {
		String sql="select userId,name,phone,email,address,loginName,role,loginStatus from user where "+propName+"=?";
		return getJdbcTemplate().query(sql,new UserRowMapper(),propValue);
	}

 
}
