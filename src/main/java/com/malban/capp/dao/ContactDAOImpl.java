package com.malban.capp.dao;

import com.malban.capp.domain.Contact;
import com.malban.capp.rm.ContactRowMapper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author HHA
 */
@Repository
public class ContactDAOImpl extends BaseDAO implements ContactDAO {
	
	public void save(Contact c) {

		String sql = "insert into contact(userId,name,phone,email,adress,remark) "
				+ "values(:userId,:name,:phone,:email,:address,:remark)";
		Map m = new HashMap();
		m.put("userId", c.getUserId());
		m.put("name", c.getName());
		m.put("phone", c.getPhone());
		m.put("email", c.getEmail());
		m.put("address", c.getAdress());
		m.put("remark", c.getRemark());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		KeyHolder kh = new GeneratedKeyHolder();
		super.getNamedParameterJdbcTemplate().update(sql, ps, kh);
		Integer contactId = kh.getKey().intValue();
		c.setContactId(contactId);

	}

	public void update(Contact c) {

		String sql = " update contact " + " set name=:name, " + " phone=:phone, " + " email=:email, "
				+ " adress=:adress, " + " remark=:remark " + " where contactId=:contactId";
		Map m = new HashMap();
		m.put("name", c.getName());
		m.put("phone", c.getPhone());
		m.put("email", c.getEmail());
		m.put("adress", c.getAdress());
		m.put("remark", c.getRemark());
		m.put("contactId", c.getContactId());
		getNamedParameterJdbcTemplate().update(sql, m);

	}

	public void delete(Contact c) {
		this.delete(c.getContactId());
	}

	public void delete(Integer contactId) {
		String sql="delete from contact where contactId=?";
		getJdbcTemplate().update(sql, contactId);
		
	}

	public Contact findById(Integer contactId) {
		String sql="select contactId,userId,name,phone,email,adress,remark from contact where contactId=?";
		Contact c=getJdbcTemplate().queryForObject(sql, new ContactRowMapper(), contactId);
		return c;
	}

	public List<Contact> findAll() {
		String sql="select contactId,userId,name,phone,email,address,remark from contact";
		return getJdbcTemplate().query(sql, new ContactRowMapper());
	}

	public List<Contact> findByProperty(String propName, Object propValue) {
		String sql="select contactId,userId,name,phone,email,adress,remark from contact where "+propName+"=?";
		return getJdbcTemplate().query(sql, new ContactRowMapper(),propValue);
	}


 
}
